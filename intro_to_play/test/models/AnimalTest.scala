package models

import org.scalatest.FunSuite

class AnimalTest extends FunSuite {

  val imageUrl = "http://example.com/image.jpg"
  val animalName = "cow"

  test("An animal's name must be a non-blank string") {
    intercept[IllegalArgumentException] {
      new Animal("", imageUrl)
    }
  }

  test("An animal's image URL must begin with 'http'") {
    intercept[IllegalArgumentException] {
      new Animal(animalName, imageUrl = "not_http")
    }
  }

}

