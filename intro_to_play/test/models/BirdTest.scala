package models

import org.scalatest.FunSuite

import Bird.nodeToBird

import scala.xml.Elem

class BirdTest extends FunSuite {

  test("this test will always pass") {
    assert(!false)
  }

  val xml: Elem =
    <animals>
      <ampbibians/>
      <reptiles/>
      <mammals/>
      <birds>
        <bird name="Common Crane" image="http://localhost:9000/assets/images/common_crane.jpg"/>
        <bird name="Ladder-Backed Woodpecker" image="http://localhost:9000/assets/images/ladderbacked_woodpecker.jpg"/>
      </birds>
    </animals>

  test("XML bird elements are correctly mapped to Bird instances") {
    val birds = (xml \\ "animals" \ "birds" \ "bird").map(nodeToBird)
    assert(birds.size === 2)

    for (i <- 0 to 1) {
      val bird = birds(i)
      i match {
        case 0 => {
          assert(bird.name === "Common Crane")
          assert(bird.imageUrl === "http://localhost:9000/assets/images/common_crane.jpg")
        }
        case 1 => {
          assert(bird.name === "Ladder-Backed Woodpecker")
          assert(bird.imageUrl === "http://localhost:9000/assets/images/ladderbacked_woodpecker.jpg")
        }
      }
    }
  }


}
