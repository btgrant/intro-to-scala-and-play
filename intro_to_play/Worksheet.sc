case class Animal(val name: String)

class Bird(birdName: String)
  extends Animal(birdName)
val animal = new Animal("cow")
val bird = new Bird("pigeon") with Flight

val duck = new Bird("duck") with Flight with Swimming
bird.name
bird.takeOff
bird.land

duck.swim

trait Flight {
  val name: String

  def takeOff {
    println(s"$name is taking off")
  }

  def land {
    println(s"$name is landing")
  }
}

trait Swimming {
  val name: String

  def swim: Unit = {
    println(s"$name is swimming")
  }
}
