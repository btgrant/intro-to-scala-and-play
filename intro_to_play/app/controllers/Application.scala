package controllers

import models.{Animal, Bird}
import play.api.mvc._
import services.BirdService
import views.html.all_birds

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Application extends Controller {

  def allBirds = Action.async {

    val futureBirds: Future[List[Bird]] = BirdService.fetchBirds

    futureBirds.map { birds: List[Bird] =>
      Ok(all_birds.render("All Birds", birds.sortWith(Animal.animalsSortedByName)))
    }
  }

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

}
