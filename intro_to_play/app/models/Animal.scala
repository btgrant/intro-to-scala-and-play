package models

import org.apache.commons.lang3.StringUtils

case class Animal(name: String, imageUrl: String) {
  require(StringUtils.isNotBlank(name), "animals must have valid names")
  require(StringUtils.isNotBlank(imageUrl) && imageUrl.startsWith("http"))
}

object Animal {

  def animalsSortedByName(x: Animal, y: Animal) = {
    x.name < y.name
  }

}
