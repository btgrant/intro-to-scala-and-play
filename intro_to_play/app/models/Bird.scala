package models

import scala.xml.Node

class Bird(birdName: String, birdHabitatNames: Set[String], imageUrl: String)
  extends Animal(name = birdName, imageUrl = imageUrl) {
  require(birdHabitatNames != null)
}

object Bird {

  def nameFromNode(n: Node): String = {
    (n \ "@name").text
  }

  def nodeToBird(n: Node): Bird = {
    val name = (n \ "@name").text
    val imageUrl = (n \ "@image").text

    val habitatNamesSet = (n \ "habitats" \ "habitat" ).map { nameFromNode }.toSet
    new Bird(name, habitatNamesSet, imageUrl)
  }

}
