package services

import models.Bird
import models.Bird.nodeToBird
import play.api.Logger
import play.api.Play.current
import play.api.libs.ws.{WS, WSResponse}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object BirdService {

  def fetchBirds: Future[List[Bird]] = {

    Logger.info("Bird Service:  fetching birds")

    val birdResponseFuture: Future[WSResponse] = WS.url("http://localhost:9001/animals")
      .withRequestTimeout(4000)
      .get()

    Logger.info("Bird Service:  got birds future")

    birdResponseFuture.map { response: WSResponse =>
      val birdList: List[Bird] = (response.xml \\ "animals" \ "birds" \ "bird").map(nodeToBird).toList

      Logger.info(s"birds fetched are ${birdList.mkString(", ")}")

      birdList
    }
  }
}
