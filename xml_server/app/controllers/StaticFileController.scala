package controllers

import play.api.Play
import play.api.Play.current
import play.api.mvc.Controller

object StaticFileController extends Controller {

  def file(fileName: String) = {

    booleanConfigFor(s"${fileName}_delay")
    .filter {delay =>
      println(s"delay is $delay")
      delay}
    .map {_ =>
      val time: Int = intConfigFor(s"${fileName}_delay_time").getOrElse(5000)
      println(s"delaying ${fileName} for ${time}")
      Thread.sleep(time)
      time
    }

    Assets.at("/public", fileName)
  }

  def another = {
    Assets.at("/public", "habitats.xml")
  }

  def booleanConfigFor(key: String) = {
    Play.application.configuration.getBoolean(key)
  }

  def intConfigFor(key: String) = {
    Play.application.configuration.getInt(key)
  }

}
